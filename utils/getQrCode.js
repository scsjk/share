var config={};
const appid ='wx85a8ff8327b8e239';
const secret ='b2b174f766d66c0700a12b5c6033fa24';

config.getToken=function(callback){
  wx.request({
    url: 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' + appid + '&secret=' + secret,
    success(res) {
      config.getQrcode(res.data.access_token, callback);
    }
  })
}
config.getQrcode = function (access_token, callback){
  wx.request({
    url: 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' + access_token,
    method: "POST",
    responseType: 'arraybuffer',
    data: {
      scene: "85688",
      path: 'pages/index/index',
      width: '430px',
      auto_color: true,
      line_color: { "r": "0", "g": "0", "b": "0" },
      is_hyaline: false
    },
    header: {
      'content-type': 'application/json;charset=utf-8'
    },
    success(res) {
      let base64 = wx.arrayBufferToBase64(res.data);
      let base64Data = "data:image/png;base64," + base64;
      callback(base64Data);
    }
  })
}

function getBase64ImgData(){
  return new Promise((resolve,reject)=>{
    config.getToken(function(data){
      resolve(data);
    });
  });
}
module.exports={
  getBase64ImgData: getBase64ImgData
}